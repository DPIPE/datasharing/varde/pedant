package main

import (
	"context"
	"io"
	"log"
	"log/slog"
)

type GNUErrorHandlerOptions struct {
	SlogOpts slog.HandlerOptions
}

type GNUErrorHandler struct {
	h slog.Handler
	l *log.Logger
}

func (h *GNUErrorHandler) Handle(ctx context.Context, r slog.Record) error {
	type fl struct {
		file string
		line int64
	}
	fileline := fl{}
	switch r.Level {
	case slog.LevelError:
		r.Attrs(func(a slog.Attr) bool {
			switch a.Key {
			case "file":
				fileline.file = a.Value.String()
			case "line":
				fileline.line = a.Value.Int64()
			}
			return true
		})
		if fileline.line == 0 {
			h.l.Printf("%s: %s\n", fileline.file, r.Message)
		} else {
			h.l.Printf("%s:%d: %s\n", fileline.file, fileline.line, r.Message)
		}
	default:
		h.l.Printf("%s: %s\n", r.Level, r.Message)
	}
	return nil
}

func (h *GNUErrorHandler) Enabled(ctx context.Context, level slog.Level) bool {
	return h.h.Enabled(ctx, level)
}

func (h *GNUErrorHandler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return &GNUErrorHandler{h: h.h.WithAttrs(attrs), l: h.l}
}

func (h *GNUErrorHandler) WithGroup(name string) slog.Handler {
	return &GNUErrorHandler{h: h.h.WithGroup(name)}
}

func NewGNUErrorHandler(w io.Writer, opts *slog.HandlerOptions) *GNUErrorHandler {
	if opts == nil {
		opts = &slog.HandlerOptions{}
	}
	return &GNUErrorHandler{
		h: slog.NewTextHandler(w, &slog.HandlerOptions{
			Level:     opts.Level,
		}),
		l: log.New(w, "", 0),
	}
}
