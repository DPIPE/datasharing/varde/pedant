package main

import (
	"bytes"
	"io"
	"log/slog"
	"os"

	"github.com/brentp/vcfgo"
	"gitlab.com/DPIPE/datasharing/varde/pedant/internal/header"
	"gitlab.com/DPIPE/datasharing/varde/pedant/internal/records"
)

type ValidateCmd struct {
	Vcf       string `arg:"" required:"" type:"existingfile" help:"Vcf file to validate"`
	Reference string `short:"r" type:"existingfile" help:"Reference fasta for checking that reference alleles in the vcf match the reference genome at the same position"`
}

func (c *ValidateCmd) Run() error {
	vcf, err := os.Open(c.Vcf)
	if err != nil {
		return err
	}
	slog.Debug("File opened", "file", c.Vcf)
	defer vcf.Close()
	var buf bytes.Buffer
	tee := io.TeeReader(vcf, &buf)
	rdr, err := vcfgo.NewReader(tee, false)
	if err != nil {
		slog.Error(err.Error(), "file", c.Vcf)
		return err
	}
	defer rdr.Close()
	if err := header.FileFormatValid(rdr.Header); err != nil {
		slog.Error(err.Error(), "file", c.Vcf)
		return err
	}
	headerValid, err := header.HeaderContigValid(rdr.Header)
	if !headerValid {
		slog.Error(err.Error(), "file", c.Vcf)
		return err
	}
	infosValid, err := header.HeaderInfoValid(rdr.Header)
	if !infosValid {
		slog.Error(err.Error(), "file", c.Vcf)
		return err
	}
	lineNumber, err := records.RecordsValid(rdr)
	if err != nil {
		slog.Error(err.Error(), "line", lineNumber, "file", c.Vcf)
		return err
	}
	// If a reference fasta is provided,
	// we check that the REF field of each variant corresponds to the reference.
	if c.Reference != "" {
		reference, err := os.Open(c.Reference)
		if err != nil {
			slog.Error(err.Error(), "file", c.Reference)
			return err
		}
		defer reference.Close()
		rdr2, err := vcfgo.NewReader(&buf, false)
		if err != nil {
			slog.Error(err.Error(), "file", c.Reference)
			return err
		}
		failedRecords, err := records.CheckReferenceSequence(rdr2, reference)
		if err != nil {
			for _, failedRecord := range failedRecords {
				slog.Error(failedRecord.Err.Error(), "line", failedRecord.Line, "file", c.Vcf)
			}
			slog.Error(err.Error(), "file", c.Vcf)
			return err
		}
	}
	slog.Info("Validation complete")
	return nil
}
