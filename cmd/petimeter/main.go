package main

import (
	"io"
	"log/slog"
	"os"

	"github.com/alecthomas/kong"
)

type debugFlag bool

func (d debugFlag) AfterApply(lvl *slog.LevelVar) error {
	if d {
		lvl.Set(slog.LevelDebug)
	}
	return nil
}

var cli struct {
	Debug     debugFlag   `help:"Enable debug mode"`
	LogFormat string      `enum:"json,gnu,text" default:"gnu" help:"Set output logging format, 'gnu', 'text' or 'json'"`
	Validate  ValidateCmd `cmd:"validate" help:"Validate vcf file"`
}

func main() {
	// Set up structured logging
	lvl := new(slog.LevelVar)

	ctx := kong.Parse(&cli,
		kong.UsageOnError(),
		kong.Description("Utility for verifying variant interpretation VCFs"),
		kong.Bind(lvl),
	)

	slog.SetDefault(slog.New(setLogHandler(cli.LogFormat, os.Stdout, &slog.HandlerOptions{Level: lvl, AddSource: true})))

	err := ctx.Run()
	if err != nil {
		ctx.Fatalf("Failed validation")
	}
}

// Choose different output formats at runtime
func setLogHandler(format string, w io.Writer, opts *slog.HandlerOptions) slog.Handler {
	var handler slog.Handler
	switch format {
	case "gnu":
		handler = NewGNUErrorHandler(w, opts)
	case "json":
		handler = slog.NewJSONHandler(w, opts)
	case "text":
		handler = slog.NewTextHandler(w, opts)
	default:
		handler = slog.Default().Handler()
	}
	return handler
}
