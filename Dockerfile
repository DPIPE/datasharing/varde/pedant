FROM bash:5.2.37@sha256:6b7a52601cb4a02a370b394858eb609e701bf221920a259ecb5a933c6d5b3d2e AS bash
FROM scratch 
# The following will add the minimum set of libraries to run bash
COPY --from=bash /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1 
COPY --from=bash /lib/libc.musl-x86_64.so.1 /lib/libc.musl-x86_64.so.1 
COPY --from=bash /usr/lib/libncursesw.so.6.5  /usr/lib/libncursesw.so.6.5
COPY --from=bash /usr/lib/libncursesw.so.6 /usr/lib/libncursesw.so.6
# Add bash to the expected location for nextflow
COPY --from=bash /usr/local/bin/bash /bin/bash
ENTRYPOINT ["/petimeter"]
COPY petimeter /