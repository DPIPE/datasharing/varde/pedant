# Pedant

A repository for code that verifies the correctness of variant classification files.
Responsible for:

- checking correctness of VCF format and INFO fields
- checking that there are no multi-allelic sites (or decomposing them)
- normalising indels
- checking that the REF sequence matches with the reference genome at POS
