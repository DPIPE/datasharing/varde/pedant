# Pedant

A pedantic VCF file verifier for the Varde project

Pedant verifies the input vcf based on the [variant call format specification](https://samtools.github.io/hts-specs/VCFv4.4.pdf) and specifications for the variant classification format found in the [VARDE project](https://gitlab.com/DPIPE/datasharing/varde/vardeDB/-/blob/dev/docs/specification/solution_design.md?ref_type=heads#input-format)


```sh
~> petimeter
Usage: petimeter <command> [flags]

Utility for verifying variant interpretation VCFs

Flags:
  -h, --help                Show context-sensitive help.
      --debug               Enable debug mode
      --log-format="gnu"    Set output logging format, 'gnu', 'text' or 'json'

Commands:
  validate <vcf>
    Validate vcf file

Run "petimeter <command> --help" for more information on a command.
```
## Commands

### validate
```sh
~> petimeter validate --help
Usage: petimeter validate <vcf> [flags]

Validate vcf file

Arguments:
  <vcf>    Vcf file to validate

Flags:
  -h, --help                   Show context-sensitive help.
      --debug                  Enable debug mode
      --log-format="gnu"       Set output logging format, 'gnu', 'text' or 'json'

  -r, --reference=REFERENCE    Reference fasta for checking that reference alleles in the vcf match the reference genome
```

- `petimeter validate` will provide no output and exit with 0 if the input vcf file is deemed valid
- `petimeter validate` will output file name and error message if a header error is found and exit with 1
- `petimeter validate` will output file name, linenumber and error message if a record error is found and exit with 1
- `petimeter validate` will output file name, linenumber, error and exit with 1 if a fasta file is provided with the optional `--reference` flag and one or serveral reference alleles do not match the reference at the given position

Example usage:

```sh
~> petimeter validate valid.vcf
```

```sh
~> petimeter validate testdata/records/unordered.vcf 
/workspaces/pedant/testdata/records/unordered.vcf:33: records are not ordered
petimeter: error: Failed validation
```

Reading from stdin

```sh
~> cat testdata/records/unordered.vcf  | ./petimeter validate -
/dev/stdin:33: records are not ordered
petimeter: error: Failed validation
```
Reference check

```sh
~> cat ../../testdata/records/same_pos_different_ref.vcf | ./petimeter validate - --reference /data/reference/IGSR/genome/HumanG1Kv37/human_g1k_v37.fasta
/dev/stdin:33: reference mismatch. REF is C, expected N
/dev/stdin:34: reference mismatch. REF is A, expected N
/dev/stdin: failed reference check
petimeter: error: Failed validation
```


## Adding petimeter to your docker file

```Dockerfile
# Reference pedant docker image to appease hadolint
FROM registry.gitlab.com/dpipe/datasharing/varde/pedant:v0.0.4 AS pedant
COPY --from=pedant /petimeter /usr/local/bin/petimeter
```