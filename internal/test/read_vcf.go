package test

import (
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/brentp/vcfgo"
)

// Returns a *vcfgo.Reader from a file path relative
// to the top level project directory
// Fails the test with log output from the test context
// it is called from
func VCFFromFile(t testing.TB, filename string) *vcfgo.Reader {
	t.Helper()
	f, err := os.Open(filepath.Join(Root, filename))
	if err != nil {
		t.Fatal(err)
	}
	t.Cleanup(func() {
		f.Close()
	})
	return VCFFromReader(t, f)
}

// Returns a *vcfgo.Reader from an io.Reader
// Fails the test with log output from the test context
// it is called from
func VCFFromReader(t testing.TB, rdr io.Reader) *vcfgo.Reader {
	t.Helper()
	vcf, err := vcfgo.NewReader(rdr, true)
	if err != nil {
		t.Fatal(err)
	}
	t.Cleanup(func() {
		vcf.Close()
	})
	return vcf
}
