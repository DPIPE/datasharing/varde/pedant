package test

import (
	"path/filepath"
	"runtime"
)

// Minor magicks for referencing the toplevel project path
var (
	_, b, _, _ = runtime.Caller(0)
	Root       = filepath.Join(filepath.Dir(b), "..", "..")
)
