package header

import (
	"reflect"
	"testing"

	"gitlab.com/DPIPE/datasharing/varde/pedant/internal/test"
)

func Test_HeaderContigValid(t *testing.T) {
	tests := []struct {
		vcfFile       string
		shouldBeValid bool
	}{
		{
			vcfFile:       "testdata/header/contig/valid.vcf",
			shouldBeValid: true,
		},
		{
			vcfFile:       "testdata/header/contig/missing.vcf",
			shouldBeValid: false,
		},
		{
			vcfFile:       "testdata/header/contig/incorrect_length.vcf",
			shouldBeValid: false,
		},
		{
			vcfFile:       "testdata/header/contig/length_not_int.vcf",
			shouldBeValid: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.vcfFile, func(t *testing.T) {
			valid, err := HeaderContigValid(test.VCFFromFile(t, tt.vcfFile).Header)
			if valid != tt.shouldBeValid {
				t.Fatal(err)
			}
		})
	}
}

func Test_contigIDS(t *testing.T) {
	tests := []struct {
		name    string
		vcfFile string
		want    map[string]int
		wantErr bool
	}{
		{
			vcfFile: "testdata/header/contig/single_contig.vcf",
			want:    map[string]int{"X": 1},
		},
		{
			vcfFile: "testdata/header/contig/non_integer.vcf",
			want:    map[string]int{},
			wantErr: true,
		},
		{
			vcfFile: "testdata/header/contig/duplicate_id.vcf",
			want:    map[string]int{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.vcfFile, func(t *testing.T) {
			contigs, err := contigIDS(test.VCFFromFile(t, tt.vcfFile).Header)
			if err == nil && tt.wantErr {
				t.Fatalf("Should have failed")
			}
			if !reflect.DeepEqual(tt.want, contigs) {
				t.Fatalf("Got %v, expected %v", contigs, tt.want)
			}
		})
	}
}

func Test_verifyContig(t *testing.T) {
	tests := []struct {
		name    string
		contig  map[string]string
		id      string
		length  int
		wantErr bool
	}{
		{
			name: "No length defined",
			contig: map[string]string{
				"ID": "X",
			},
			wantErr: true,
		},
		{
			name: "No ID defined",
			contig: map[string]string{
				"id":     "X",
				"length": "2",
			},
			wantErr: true,
		},
		{
			name: "Length not an integer",
			contig: map[string]string{
				"ID":     "X",
				"length": "a",
			},
			id:      "X",
			wantErr: true,
		},
		{
			name: "Valid contig",
			contig: map[string]string{
				"ID":     "X",
				"length": "23",
			},
			id:      "X",
			length:  23,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			id, length, err := verifyContig(tt.contig)
			if err == nil && tt.wantErr { // No error returned, but we wanted one
				t.Fatalf("Should have returned an error")
			} else {
				if id != tt.id {
					t.Fatalf("ID: Got %s, wanted %s (%v)", id, tt.id, tt.contig)
				}
				if length != tt.length {
					t.Fatalf("length: Got %d, wanted %d (%v)", length, tt.length, tt.contig)
				}
			}
		})
	}
}
