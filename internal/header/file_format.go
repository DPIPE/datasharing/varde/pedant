package header

import (
	"fmt"
	"log/slog"

	"github.com/brentp/vcfgo"
)

const ValidFileFormat = "4.4"

func FileFormatValid(header *vcfgo.Header) error {
	slog.Debug("Validating file version definition")
	if header.FileFormat != ValidFileFormat {
		return fmt.Errorf("file format version is: %q, should be: %q", header.FileFormat, ValidFileFormat)
	}
	return nil
}
