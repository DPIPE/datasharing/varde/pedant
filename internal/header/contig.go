package header

import (
	"errors"
	"fmt"
	"log/slog"
	"slices"
	"strconv"

	"github.com/brentp/vcfgo"
	"golang.org/x/exp/maps"
)

var validContigs = map[string]int{
	"1":  249250621,
	"2":  243199373,
	"3":  198022430,
	"4":  191154276,
	"5":  180915260,
	"6":  171115067,
	"7":  159138663,
	"8":  146364022,
	"9":  141213431,
	"10": 135534747,
	"11": 135006516,
	"12": 133851895,
	"13": 115169878,
	"14": 107349540,
	"15": 102531392,
	"16": 90354753,
	"17": 81195210,
	"18": 78077248,
	"19": 59128983,
	"20": 63025520,
	"21": 48129895,
	"22": 51304566,
	"X":  155270560,
	"Y":  59373566,
	"MT": 16569,
}

func HeaderContigValid(header *vcfgo.Header) (bool, error) {
	slog.Debug("Validating contigs in header")
	contigIDS, err := contigIDS(header)
	if err != nil {
		return false, err
	}
	for id, validLength := range validContigs {
		length, ok := contigIDS[id]
		if !ok {
			return false, fmt.Errorf("contigs does not contain ID %s", id)
		}
		if length != validLength {
			return false, fmt.Errorf("contig ID %s is %d, but should be %d", id, length, validLength)
		}
	}
	return true, nil
}

// Read the contigs and map them to ID:length
// Return an error if verifyContig fails
// Return an error if contig ID is duplicated
func contigIDS(header *vcfgo.Header) (map[string]int, error) {
	ret := map[string]int{}
	for _, contig := range header.Contigs {
		id, length, err := verifyContig(contig)
		if err != nil {
			return ret, err
		}
		_, exists := ret[id]
		if exists {
			return map[string]int{}, fmt.Errorf("duplicate contig ID entry for %s", id)
		}
		ret[id] = length
	}
	return ret, nil
}

// Take a contig entry and return the ID and length
// Return an error if length is not an integer
// Return an error if length is not defined (it is optional in the standard, but we want it)
// Return if ID is not a key
func verifyContig(contig map[string]string) (key string, length int, err error) {
	keys := maps.Keys(contig)
	if !slices.Contains(keys, "length") {
		return key, length, errors.New("contig does not contain length entry")
	}
	if !slices.Contains(keys, "ID") { // Handled by vcfgo as well
		return key, length, errors.New("contig does not contain ID entry")
	}
	key = contig["ID"]
	length, err = strconv.Atoi(contig["length"])
	if err != nil {
		return key, length, fmt.Errorf("contig ID %s has length of \"%s\", which is not an integer", key, contig["length"])
	}
	return key, length, err
}
