package header

import (
	"reflect"
	"testing"

	"github.com/brentp/vcfgo"
	"gitlab.com/DPIPE/datasharing/varde/pedant/internal/test"
)

func Test_HeaderInfoValid(t *testing.T) {
	tests := []struct {
		vcfFile       string
		shouldBeValid bool
	}{
		{
			vcfFile:       "testdata/header/info/minimal_passing.vcf",
			shouldBeValid: true,
		},
		{
			vcfFile:       "testdata/header/info/missing_class.vcf",
			shouldBeValid: false,
		},
		{
			vcfFile:       "testdata/header/info/lowercase_class.vcf",
			shouldBeValid: false,
		},
		{
			vcfFile:       "testdata/header/info/missing_year.vcf",
			shouldBeValid: false,
		},
		{
			vcfFile:       "testdata/header/info/missing_pubmed.vcf",
			shouldBeValid: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.vcfFile, func(t *testing.T) {
			valid, err := HeaderInfoValid(test.VCFFromFile(t, tt.vcfFile).Header)
			if valid != tt.shouldBeValid {
				t.Fatalf("Expected %t, got %t. Error: %s", tt.shouldBeValid, valid, err)
			}
		})
	}
}

func Test_infoIDS(t *testing.T) {
	tests := []struct {
		vcfFile string
		want    map[string]*vcfgo.Info
		wantErr bool
	}{
		{
			vcfFile: "testdata/header/info/minimal_passing.vcf",
			want: map[string]*vcfgo.Info{
				"CLASS": {
					Id:          "CLASS",
					Description: "Variant classification",
					Number:      "1",
					Type:        "String",
				},
				"YEAR": {
					Id:          "YEAR",
					Description: "Year of interpretation",
					Number:      "1",
					Type:        "Integer",
				},
				"ACMG": {
					Id:          "ACMG",
					Description: "ACMG criteria",
					Number:      ".",
					Type:        "String",
				},
				"SVLEN": {
					Id:          "SVLEN",
					Description: "Structural variant length",
					Number:      ".",
					Type:        "Integer",
				},
				"PUBMED": {
					Id:          "PUBMED",
					Description: "List of Pubmed IDs",
					Number:      ".",
					Type:        "String",
				},
			},
			wantErr: false,
		},
		{
			vcfFile: "testdata/header/info/duplicate_mix_case.vcf",
			want:    map[string]*vcfgo.Info{},
			wantErr: true,
		},
		{
			vcfFile: "testdata/header/info/lowercase_class.vcf",
			want:    map[string]*vcfgo.Info{},
			wantErr: true,
		},
		{
			vcfFile: "testdata/header/info/lowercase_pubmed.vcf",
			want:    map[string]*vcfgo.Info{},
			wantErr: true,
		},
		{
			vcfFile: "testdata/header/info/optional_info_mix_case.vcf",
			want: map[string]*vcfgo.Info{
				"CLASS": {
					Id:          "CLASS",
					Description: "Variant classification",
					Number:      "1",
					Type:        "String",
				},
				"YEAR": {
					Id:          "YEAR",
					Description: "Year of interpretation",
					Number:      "1",
					Type:        "Integer",
				},
				"ACMG": {
					Id:          "ACMG",
					Description: "ACMG criteria",
					Number:      ".",
					Type:        "String",
				},
				"SVLEN": {
					Id:          "SVLEN",
					Description: "Structural variant length",
					Number:      ".",
					Type:        "Integer",
				},
				"PUBMED": {
					Id:          "PUBMED",
					Description: "List of Pubmed IDs",
					Number:      ".",
					Type:        "String",
				},
				"GNOMEN": {
					Id:          "gNomen",
					Description: "gDNA-level HGVS nomenclature",
					Number:      "1",
					Type:        "String",
				},
			},
			wantErr: false,
		},
		{
			vcfFile: "testdata/header/info/mixed_case_svlen.vcf",
			want:    map[string]*vcfgo.Info{},
			wantErr: true,
		},
		{
			vcfFile: "testdata/header/info/lowercase_acmg.vcf",
			want:    map[string]*vcfgo.Info{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.vcfFile, func(t *testing.T) {
			infos, err := infoIDS(test.VCFFromFile(t, tt.vcfFile).Header)
			if (err != nil) != tt.wantErr {
				t.Fatalf("Should have failed")
			}
			if !reflect.DeepEqual(tt.want, infos) {
				t.Fatalf("Got %v, expected %v", infos, tt.want)
			}
		})
	}
}

func Test_verifyInfo(t *testing.T) {
	tests := []struct {
		name       string
		info       *vcfgo.Info
		shouldFail bool
	}{
		{
			name: "We pass unrelated INFOS",
			info: &vcfgo.Info{
				Id:     "gNomen",
				Number: "1",
				Type:   "String",
			},
			shouldFail: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := verifyInfo(tt.info)
			if err == nil && tt.shouldFail {
				t.Fatal(err)
			}
		})
	}
}

func Test_verifyClassInfo(t *testing.T) {
	tests := []struct {
		name       string
		info       *vcfgo.Info
		shouldFail bool
	}{
		{
			name: "Valid",
			info: &vcfgo.Info{
				Id:          "CLASS",
				Number:      "1",
				Type:        "String",
				Description: "Variant classification",
			},
			shouldFail: false,
		},
		{
			name: "Invalid description",
			info: &vcfgo.Info{
				Id:          "CLASS",
				Number:      "1",
				Type:        "String",
				Description: "Classification",
			},
			shouldFail: true,
		},
		{
			name: "non uppercase CLASS",
			info: &vcfgo.Info{
				Id:          "class",
				Number:      "1",
				Type:        "String",
				Description: "Variant classification",
			},
			shouldFail: true,
		},
		{
			name: "CLASS Number not 1",
			info: &vcfgo.Info{
				Id:          "CLASS",
				Number:      "2",
				Type:        "String",
				Description: "Variant classification",
			},
			shouldFail: true,
		},
		{
			name: "CLASS Type not String",
			info: &vcfgo.Info{
				Id:          "CLASS",
				Number:      "1",
				Type:        "Integer",
				Description: "Variant classification",
			},
			shouldFail: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := verifyClassInfo(tt.info)
			if err == nil && tt.shouldFail {
				t.Fatal(err)
			}
			if err != nil && !tt.shouldFail {
				t.Fatal(err)
			}
		})
	}
}

func Test_verifyACMGInfo(t *testing.T) {
	tests := []struct {
		name       string
		info       *vcfgo.Info
		shouldFail bool
	}{
		{
			name: "Passable ACMG",
			info: &vcfgo.Info{
				Id:          "ACMG",
				Number:      ".",
				Type:        "String",
				Description: "ACMG criteria",
			},
			shouldFail: false,
		},
		{
			name: "non uppercase ACMG",
			info: &vcfgo.Info{
				Id:          "acmg",
				Number:      ".",
				Type:        "String",
				Description: "ACMG criteria",
			},
			shouldFail: true,
		},
		{
			name: "ACMG Number not .",
			info: &vcfgo.Info{
				Id:          "ACMG",
				Number:      "2",
				Type:        "String",
				Description: "ACMG criteria",
			},
			shouldFail: true,
		},
		{
			name: "ACMG Type not String",
			info: &vcfgo.Info{
				Id:          "ACMG",
				Number:      ".",
				Type:        "Integer",
				Description: "ACMG criteria",
			},
			shouldFail: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := verifyACMGInfo(tt.info)
			if err == nil && tt.shouldFail {
				t.Fatal(err)
			}
			if err != nil && !tt.shouldFail {
				t.Fatal(err)
			}
		})
	}
}

func Test_verifyPUBMEDInfo(t *testing.T) {

	tests := []struct {
		name    string
		i       *vcfgo.Info
		wantErr bool
	}{
		{
			name: "Valid",
			i: &vcfgo.Info{
				Id:          "PUBMED",
				Number:      ".",
				Type:        "String",
				Description: "List of Pubmed IDs",
			},
			wantErr: false,
		},
		{
			name: "Invalid number",
			i: &vcfgo.Info{
				Id:          "PUBMED",
				Number:      "4",
				Type:        "String",
				Description: "List of Pubmed IDs",
			},
			wantErr: true,
		},
		{
			name: "Invalid type",
			i: &vcfgo.Info{
				Id:          "PUBMED",
				Number:      ".",
				Type:        "Integer",
				Description: "List of Pubmed IDs",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := verifyPUBMEDInfo(tt.i); (err != nil) != tt.wantErr {
				t.Errorf("verifyPUBMEDInfo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_verifySVLENInfo(t *testing.T) {
	tests := []struct {
		name    string
		i       *vcfgo.Info
		wantErr bool
	}{
		{
			name: "Valid",
			i: &vcfgo.Info{
				Id:          "SVLEN",
				Number:      ".",
				Type:        "Integer",
				Description: "Structural variant length",
			},
			wantErr: false,
		},
		{
			name: "Invalid number",
			i: &vcfgo.Info{
				Id:          "SVLEN",
				Number:      "1",
				Type:        "Integer",
				Description: "Structural variant length",
			},
			wantErr: true,
		},
		{
			name: "Invalid type",
			i: &vcfgo.Info{
				Id:          "SVLEN",
				Number:      ".",
				Type:        "String",
				Description: "Structural variant length",
			},
			wantErr: true,
		},
		{
			name: "Invalid description",
			i: &vcfgo.Info{
				Id:          "SVLEN",
				Number:      ".",
				Type:        "Integer",
				Description: "SV length",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := verifySVLENInfo(tt.i); (err != nil) != tt.wantErr {
				t.Errorf("verifySVLENInfo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_verifyYEARInfo(t *testing.T) {
	tests := []struct {
		name    string
		i       *vcfgo.Info
		wantErr bool
	}{
		{
			name: "Valid",
			i: &vcfgo.Info{
				Id:          "YEAR",
				Number:      "1",
				Type:        "Integer",
				Description: "Year of interpretation",
			},
			wantErr: false,
		},
		{
			name: "Id not YEAR",
			i: &vcfgo.Info{
				Id:          "Year",
				Number:      "1",
				Type:        "Integer",
				Description: "Year of interpretation",
			},
			wantErr: true,
		},
		{
			name: "Invalid number",
			i: &vcfgo.Info{
				Id:          "YEAR",
				Number:      ".",
				Type:        "Integer",
				Description: "Year of interpretation",
			},
			wantErr: true,
		},
		{
			name: "Invalid type",
			i: &vcfgo.Info{
				Id:          "YEAR",
				Number:      "1",
				Type:        "String",
				Description: "Year of interpretation",
			},
			wantErr: true,
		},
		{
			name: "Missing description",
			i: &vcfgo.Info{
				Id:     "YEAR",
				Number: "1",
				Type:   "Integer",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := verifyYEARInfo(tt.i); (err != nil) != tt.wantErr {
				t.Errorf("verifyYEARInfo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
