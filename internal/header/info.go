package header

import (
	"fmt"
	"log/slog"
	"strings"

	"github.com/brentp/vcfgo"
	"gitlab.com/DPIPE/datasharing/varde/pedant/internal/info"
)

var RequiredInfos = []string{info.CLASS, info.ACMG, info.PUBMED, info.SVLEN, info.YEAR}

func HeaderInfoValid(header *vcfgo.Header) (bool, error) {
	slog.Debug("Validating infos in header")
	infos, err := infoIDS(header)
	if err != nil {
		return false, err
	}
	// The infos present that we have an opinion about are valid,
	// but we still need to check for presence of CLASS

	for _, info := range RequiredInfos {
		_, ok := infos[info]
		if !ok {
			return false, fmt.Errorf("INFO %s must be present", info)
		}
	}

	return true, nil
}

// infoIDS checks the vcf header for any ducplicate infos
func infoIDS(header *vcfgo.Header) (map[string]*vcfgo.Info, error) {
	ret := map[string]*vcfgo.Info{}
	for _, info := range header.Infos {
		// first, check for duplicate entries by mixedCase
		upcaseId := strings.ToUpper(info.Id)
		found, exists := ret[upcaseId]
		if exists {
			return map[string]*vcfgo.Info{}, fmt.Errorf("duplicate or mixedCase duplicate entry for %s (allready saw %s)", info.Id, found.Id)
		}
		// Then verify the entry
		err := verifyInfo(info)
		if err != nil {
			return map[string]*vcfgo.Info{}, err
		}
		ret[upcaseId] = info
	}
	return ret, nil
}

func verifyInfo(i *vcfgo.Info) error {
	switch strings.ToUpper(i.Id) {
	case info.CLASS:
		return verifyClassInfo(i)
	case info.ACMG:
		return verifyACMGInfo(i)
	case info.PUBMED:
		return verifyPUBMEDInfo(i)
	case info.SVLEN:
		return verifySVLENInfo(i)
	case info.YEAR:
		return verifyYEARInfo(i)
	}
	return nil
}

func verifyClassInfo(i *vcfgo.Info) error {
	if i.Id != info.CLASS {
		return fmt.Errorf("CLASS must be uppercase, not %s", i.Id)
	}
	if i.Number != "1" {
		return fmt.Errorf("CLASS Number must be 1, not %s", i.Number)
	}
	if i.Type != "String" {
		return fmt.Errorf("CLASS Type must be String, not %s", i.Type)
	}
	if i.Description != "Variant classification" {
		return fmt.Errorf("%s description should be 'Variant classification', not '%s'", info.CLASS, i.Description)
	}
	return nil
}

func verifyACMGInfo(i *vcfgo.Info) error {
	if i.Id != info.ACMG {
		return fmt.Errorf("%s must be uppercase, not '%s'", info.ACMG, i.Id)
	}
	if i.Number != "." {
		return fmt.Errorf("%s Number should be defined as ., not '%s'", info.ACMG, i.Number)
	}
	if i.Type != "String" {
		return fmt.Errorf("%s Type should be String, not '%s'", info.ACMG, i.Type)
	}
	if i.Description != "ACMG criteria" {
		return fmt.Errorf("%s description should be 'ACMG criteria', not '%s'", info.ACMG, i.Description)
	}
	return nil
}

func verifyPUBMEDInfo(i *vcfgo.Info) error {
	if i.Id != info.PUBMED {
		return fmt.Errorf("%s must be uppercase, not %s", info.PUBMED, i.Id)
	}
	if i.Number != "." {
		return fmt.Errorf("%s Number should be defined as ., not '%s'", info.PUBMED, i.Number)
	}
	if i.Type != "String" {
		return fmt.Errorf("%s Type should be String, not '%s'", info.PUBMED, i.Type)
	}
	if i.Description != "List of Pubmed IDs" {
		return fmt.Errorf("%s description should be 'List of Pubmed IDs', not '%s'", info.PUBMED, i.Description)
	}
	return nil
}

func verifySVLENInfo(i *vcfgo.Info) error {
	if i.Id != info.SVLEN {
		return fmt.Errorf("%s must be uppercase, not %s", info.SVLEN, i.Id)
	}
	if i.Number != "." {
		return fmt.Errorf("%s Number should be defined as ., not '%s'", info.SVLEN, i.Number)
	}
	if i.Type != "Integer" {
		return fmt.Errorf("%s Type should be Integer, not '%s'", info.SVLEN, i.Type)
	}
	if i.Description != "Structural variant length" {
		return fmt.Errorf("%s description should be 'Structural variant length', not '%s'", info.SVLEN, i.Description)
	}
	return nil
}

func verifyYEARInfo(i *vcfgo.Info) error {
	if i.Id != info.YEAR {
		return fmt.Errorf("%s must be uppercase, not %s", info.YEAR, i.Id)
	}
	if i.Number != "1" {
		return fmt.Errorf("%s Number should be defined as ., not '%s'", info.YEAR, i.Number)
	}
	if i.Type != "Integer" {
		return fmt.Errorf("%s Type should be Integer, not '%s'", info.YEAR, i.Type)
	}
	if i.Description != "Year of interpretation" {
		return fmt.Errorf("%s description should be 'Year of interpretation', not '%s'", info.YEAR, i.Description)
	}
	return nil
}
