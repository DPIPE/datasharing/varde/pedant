package header

import (
	"testing"

	"github.com/brentp/vcfgo"
)

func TestFileFormatValid(t *testing.T) {
	tests := []struct {
		name    string
		header  *vcfgo.Header
		wantErr bool
	}{
		{
			name: "Valid file format",
			header: &vcfgo.Header{
				FileFormat: "4.4",
			},
			wantErr: false,
		},
		{
			name: "Invalid file format",
			header: &vcfgo.Header{
				FileFormat: "3.4",
			},
			wantErr: true,
		},
		{
			name: "Empty file format",
			header: &vcfgo.Header{
				FileFormat: "",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := FileFormatValid(tt.header); (err != nil) != tt.wantErr {
				t.Errorf("FileFormatValid() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
