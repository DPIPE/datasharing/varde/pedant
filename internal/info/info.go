package info

import (
	"fmt"
	"slices"
	"strings"

	"github.com/brentp/vcfgo"
)

const (
	CLASS  = "CLASS"
	ACMG   = "ACMG"
	SVLEN  = "SVLEN"
	PUBMED = "PUBMED"
	YEAR   = "YEAR"
)

// ValidInfoField will check the info field of a variant and return an error if invalid
func ValidInfoField(variant *vcfgo.Variant) (int64, error) {
	// make sure all info IDs are defined in header/meta section
	if err := infoIdInHeader(variant); err != nil {
		return variant.LineNumber, err
	}
	// Make sure reserved info IDs do not exist in mixed case or lowercase
	if err := checkCase(variant); err != nil {
		return variant.LineNumber, err
	}
	info := variant.Info()
	// Check validity of variant's CLASS
	if classInfo, err := info.Get(CLASS); err != nil || !ValidClass(classInfo.(string)) {
		return variant.LineNumber, fmt.Errorf("CLASS missing or invalid")
	}
	// Get all info tags present for the given variant
	infoKeys := variant.Info().Keys()
	// Look for optional tags YEAR, ACMG and SVLEN and check validity if present
	if slices.Contains(infoKeys, YEAR) {
		// Get YEAR tag value and fail if it is invalid
		if year, err := info.Get(YEAR); err != nil || !ValidYear(year.(int)) {
			return variant.LineNumber, fmt.Errorf("YEAR invalid")
		}
	}
	if slices.Contains(infoKeys, ACMG) {
		// Get ACMG tag value and fail if it is invalid
		ACMGInfo, err := info.Get(ACMG)
		if err != nil {
			return variant.LineNumber, err
		}
		if err = ValidACMGCriteria(ACMGInfo); err != nil {
			return variant.LineNumber, err
		}
	}
	if slices.Contains(infoKeys, SVLEN) {
		// Fail if there is a SVLEN tag and the variant is not a SV
		if _, err := info.Get(SVLEN); err != nil || !isSV(variant) {
			return variant.LineNumber, fmt.Errorf("SVLEN should only be present for <DUP> or <DEL>")
		}
	}
	return variant.LineNumber, nil
}

// infoIdInHeader returns error if a variant contains a info id
// that is not defined in the vcf header
func infoIdInHeader(variant *vcfgo.Variant) error {
	for _, id := range variant.Info().Keys() {
		if _, ok := variant.Header.Infos[id]; !ok {
			return fmt.Errorf("info id %s not found in header", id)
		}
	}
	return nil
}

func checkCase(v *vcfgo.Variant) error {
	for _, key := range v.Info().Keys() {
		upperKey := strings.ToUpper(key)
		switch upperKey {
		case ACMG:
			if upperKey != key {
				return fmt.Errorf("%q is not uppercase", key)
			}
		case CLASS:
			if upperKey != key {
				return fmt.Errorf("%q is not uppercase", key)
			}
		case PUBMED:
			if upperKey != key {
				return fmt.Errorf("%q is not uppercase", key)
			}
		case SVLEN:
			if upperKey != key {
				return fmt.Errorf("%q is not uppercase", key)
			}
		case YEAR:
			if upperKey != key {
				return fmt.Errorf("%q is not uppercase", key)
			}
		}
	}
	return nil
}
