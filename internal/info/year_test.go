package info

import "testing"

func TestValidYear(t *testing.T) {
	tests := []struct {
		name string
		year int
		want bool
	}{
		{
			name: "Valid year",
			year: 2023,
			want: true,
		},
		{
			name: "Too early",
			year: 1814,
			want: false,
		},
		{
			name: "Too late",
			year: 20233,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ValidYear(tt.year); got != tt.want {
				t.Errorf("ValidYear() = %v, want %v", got, tt.want)
			}
		})
	}
}
