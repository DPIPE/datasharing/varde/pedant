package info

import (
	"fmt"
)

// Tokenize the valid criteria as consts
const (
	// Pathogenic very strong
	PVS1   = "PVS1"
	PVS1_S = "PVS1_S"
	PVS1_M = "PVS1_M"
	PVS1_P = "PVS1_P"
	// Pathogenic strong
	PS1    = "PS1"
	PS1_VS = "PS1_VS"
	PS1_M  = "PS1_M"
	PS1_P  = "PS1_P"
	PS2    = "PS2"
	PS2_VS = "PS2_VS"
	PS2_M  = "PS2_M"
	PS2_P  = "PS2_P"
	PS3    = "PS3"
	PS3_VS = "PS3_VS"
	PS3_M  = "PS3_M"
	PS3_P  = "PS3_P"
	PS4    = "PS4"
	PS4_VS = "PS4_VS"
	PS4_M  = "PS4_M"
	PS4_P  = "PS4_P"
	// Pathogenic moderate
	PM1    = "PM1"
	PM1_VS = "PM1_VS"
	PM1_S  = "PM1_S"
	PM1_P  = "PM1_P"
	PM2    = "PM2"
	PM2_VS = "PM2_VS"
	PM2_S  = "PM2_S"
	PM2_P  = "PM2_P"
	PM3    = "PM3"
	PM3_VS = "PM3_VS"
	PM3_S  = "PM3_S"
	PM3_P  = "PM3_P"
	PM4    = "PM4"
	PM4_VS = "PM4_VS"
	PM4_S  = "PM4_S"
	PM4_P  = "PM4_P"
	PM5    = "PM5"
	PM5_VS = "PM5_VS"
	PM5_S  = "PM5_S"
	PM5_P  = "PM5_P"
	PM6    = "PM6"
	PM6_VS = "PM6_VS"
	PM6_S  = "PM6_S"
	PM6_P  = "PM6_P"
	// Pathogenic supportive
	PP1    = "PP1"
	PP1_VS = "PP1_VS"
	PP1_S  = "PP1_S"
	PP1_M  = "PP1_M"
	PP2    = "PP2"
	PP2_VS = "PP2_VS"
	PP2_S  = "PP2_S"
	PP2_M  = "PP2_M"
	PP3    = "PP3"
	PP3_VS = "PP3_VS"
	PP3_S  = "PP3_S"
	PP3_M  = "PP3_M"
	PP4    = "PP4"
	PP4_VS = "PP4_VS"
	PP4_S  = "PP4_S"
	PP4_M  = "PP4_M"
	PP5    = "PP5"
	PP5_VS = "PP5_VS"
	PP5_S  = "PP5_S"
	PP5_M  = "PP5_M"
	// Benign stand-alone
	BA1   = "BA1"
	BA1_S = "BA1_S"
	BA1_P = "BA1_P"
	// Benign strong
	BS1   = "BS1"
	BS1_A = "BS1_A"
	BS1_P = "BS1_P"
	BS2   = "BS2"
	BS2_A = "BS2_A"
	BS2_P = "BS2_P"
	BS3   = "BS3"
	BS3_A = "BS3_A"
	BS3_P = "BS3_P"
	BS4   = "BS4"
	BS4_A = "BS4_A"
	BS4_P = "BS4_P"
	// Benign supportive
	BP1   = "BP1"
	BP1_A = "BP1_A"
	BP1_S = "BP1_S"
	BP2   = "BP2"
	BP2_A = "BP2_A"
	BP2_S = "BP2_S"
	BP3   = "BP3"
	BP3_A = "BP3_A"
	BP3_S = "BP3_S"
	BP4   = "BP4"
	BP4_A = "BP4_A"
	BP4_S = "BP4_S"
	BP5   = "BP5"
	BP5_A = "BP5_A"
	BP5_S = "BP5_S"
	BP6   = "BP6"
	BP6_A = "BP6_A"
	BP6_S = "BP6_S"
	BP7   = "BP7"
	BP7_A = "BP7_A"
	BP7_S = "BP7_S"
	// NON-ACMG
	NON_ACMG = "NON-ACMG"
)

// A function that answers if a string is considered a valid ACMG criterion
var validACMGCriterion = validACMGCriterionFn()

// Create a function that wraps over a map and returns a function for
// checking wether a given ACMG criteria is valid.
func validACMGCriterionFn() func(string) bool {
	var acmgMap = map[string]bool{
		// Pathogenic very strong
		PVS1:   true,
		PVS1_S: true,
		PVS1_M: true,
		PVS1_P: true,
		// Pathogenic strong
		PS1:    true,
		PS1_VS: true,
		PS1_M:  true,
		PS1_P:  true,
		PS2:    true,
		PS2_VS: true,
		PS2_M:  true,
		PS2_P:  true,
		PS3:    true,
		PS3_VS: true,
		PS3_M:  true,
		PS3_P:  true,
		PS4:    true,
		PS4_VS: true,
		PS4_M:  true,
		PS4_P:  true,
		// Pathogenic moderate
		PM1:    true,
		PM1_VS: true,
		PM1_S:  true,
		PM1_P:  true,
		PM2:    true,
		PM2_VS: true,
		PM2_S:  true,
		PM2_P:  true,
		PM3:    true,
		PM3_VS: true,
		PM3_S:  true,
		PM3_P:  true,
		PM4:    true,
		PM4_VS: true,
		PM4_S:  true,
		PM4_P:  true,
		PM5:    true,
		PM5_VS: true,
		PM5_S:  true,
		PM5_P:  true,
		PM6:    true,
		PM6_VS: true,
		PM6_S:  true,
		PM6_P:  true,
		// Pathogenic supportive
		PP1:    true,
		PP1_VS: true,
		PP1_S:  true,
		PP1_M:  true,
		PP2:    true,
		PP2_VS: true,
		PP2_S:  true,
		PP2_M:  true,
		PP3:    true,
		PP3_VS: true,
		PP3_S:  true,
		PP3_M:  true,
		PP4:    true,
		PP4_VS: true,
		PP4_S:  true,
		PP4_M:  true,
		PP5:    true,
		PP5_VS: true,
		PP5_S:  true,
		PP5_M:  true,
		// Benign stand-alone
		BA1:   true,
		BA1_S: true,
		BA1_P: true,
		// Benign strong
		BS1:   true,
		BS1_A: true,
		BS1_P: true,
		BS2:   true,
		BS2_A: true,
		BS2_P: true,
		BS3:   true,
		BS3_A: true,
		BS3_P: true,
		BS4:   true,
		BS4_A: true,
		BS4_P: true,
		// Benign supportive
		BP1:   true,
		BP1_A: true,
		BP1_S: true,
		BP2:   true,
		BP2_A: true,
		BP2_S: true,
		BP3:   true,
		BP3_A: true,
		BP3_S: true,
		BP4:   true,
		BP4_A: true,
		BP4_S: true,
		BP5:   true,
		BP5_A: true,
		BP5_S: true,
		BP6:   true,
		BP6_A: true,
		BP6_S: true,
		BP7:   true,
		BP7_A: true,
		BP7_S: true,
		// NON-ACMG
		NON_ACMG: true,
	}
	return func(code string) bool {
		return acmgMap[code]
	}
}

func ValidACMGCriteria(codes any) error {
	switch codes := codes.(type) {
	case string:
		if !validACMGCriterion(codes) {
			return fmt.Errorf("%s is not a valid ACMG criterion", codes)
		}
	case []string:
		for _, acmg := range codes {
			if !validACMGCriterion(acmg) {
				return fmt.Errorf("%s is not a valid ACMG criterion", acmg)
			}
		}
	default:
		return fmt.Errorf("ACMGInfo type invalid type: %q", codes)
	}
	return nil
}
