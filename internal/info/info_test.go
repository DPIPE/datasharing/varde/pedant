package info

import (
	"testing"

	"github.com/brentp/vcfgo"
	"gitlab.com/DPIPE/datasharing/varde/pedant/internal/test"
)

func TestValidInfo(t *testing.T) {
	tests := []struct {
		name    string
		vcfFile string
		wantErr bool
	}{
		{
			name:    "Valid info field",
			vcfFile: "testdata/info/info/valid.vcf",
			wantErr: false,
		},
		{
			name:    "One invalid",
			vcfFile: "testdata/info/info/one_invalid.vcf",
			wantErr: true,
		},
		{
			name:    "ACMG missing",
			vcfFile: "testdata/info/info/no_ACMG.vcf",
			wantErr: false,
		},
		{
			name:    "YEAR missing",
			vcfFile: "testdata/info/info/no_YEAR.vcf",
			wantErr: false,
		},
		{
			name:    "Empty info field",
			vcfFile: "testdata/info/info/empty_info.vcf",
			wantErr: true,
		},
		{
			name:    "DUP",
			vcfFile: "testdata/info/svlen/dup.vcf",
			wantErr: false,
		},
		{
			name:    "List of ACMG",
			vcfFile: "testdata/info/info/ACMG_list.vcf",
			wantErr: false,
		},
		{
			name:    "mixedCaseACMG",
			vcfFile: "testdata/info/info/mixed_case_acmg.vcf",
			wantErr: true,
		},
		{
			name:    "mixedCaseCLASS",
			vcfFile: "testdata/info/info/mixed_case_class.vcf",
			wantErr: true,
		},
		{
			name:    "mixed case PUBMED",
			vcfFile: "testdata/info/info/mixed_case_pubmed.vcf",
			wantErr: true,
		},
		{
			name:    "mixed case SVLEN",
			vcfFile: "testdata/info/info/mixed_case_svlen.vcf",
			wantErr: true,
		},
		{
			name:    "mixed case YEAR",
			vcfFile: "testdata/info/info/mixed_case_year.vcf",
			wantErr: true,
		},
		{
			name:    "YEAR string",
			vcfFile: "testdata/info/info/year_string.vcf",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if _, err := ValidInfoField(test.VCFFromFile(t, tt.vcfFile).Read()); (err != nil) != tt.wantErr {
				t.Errorf("ValidInfo() = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_infoInHeader(t *testing.T) {
	headerNoInfo := &vcfgo.Header{}
	headerWithInfo := &vcfgo.Header{
		SampleNames: []string{},
		Infos: map[string]*vcfgo.Info{
			"CLASS": {},
			"SVLEN": {},
		},
	}

	tests := []struct {
		name    string
		variant *vcfgo.Variant
		wantErr bool
	}{
		{
			name: "Info id in header",
			variant: &vcfgo.Variant{
				Info_:  vcfgo.NewInfoByte([]byte("SVLEN=50;CLASS=2"), headerWithInfo),
				Header: headerWithInfo,
			},
			wantErr: false,
		},
		{
			name: "Info id not in header",
			variant: &vcfgo.Variant{
				Info_:  vcfgo.NewInfoByte([]byte("SVLEN=50;CLASS=2"), headerNoInfo),
				Header: headerNoInfo,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := infoIdInHeader(tt.variant); (err != nil) != tt.wantErr {
				t.Errorf("infoInHeader() = %v, wantErr = %v", err, tt.wantErr)
			}
		})
	}
}

// Define a mock of Variant, as we are only interested in the keys
// of the defined INFOs of a variant
type VariantInfoMock struct {
	k []string
}

func NewVariantMock(keys []string) *vcfgo.Variant {
	return &vcfgo.Variant{
		Info_: &VariantInfoMock{k: keys},
	}
}

func (v *VariantInfoMock) Keys() []string {
	return v.k
}

// The implementation of these are unimportant, but serves as a warning
// about creation of multi method interfaces
func (v *VariantInfoMock) Bytes() []byte                   { return []byte{} }
func (v *VariantInfoMock) Delete(string)                   {}
func (v *VariantInfoMock) Get(string) (interface{}, error) { return nil, nil }
func (v *VariantInfoMock) Set(string, interface{}) error   { return nil }
func (v *VariantInfoMock) String() string                  { return "" }

func Test_checkCase(t *testing.T) {
	tests := []struct {
		name    string
		v       *vcfgo.Variant
		wantErr bool
	}{
		{
			name: "empty variant can contain no mixedCase errors",
			v:    NewVariantMock([]string{}),
		},
		{
			name: "single ACMG",
			v:    NewVariantMock([]string{"ACMG"}),
		},
		{
			name:    "Acmg",
			v:       NewVariantMock([]string{"Acmg"}),
			wantErr: true,
		},
		{
			name:    "Class",
			v:       NewVariantMock([]string{"Class"}),
			wantErr: true,
		},
		{
			name:    "pubMED",
			v:       NewVariantMock([]string{"pubMED"}),
			wantErr: true,
		},
		{
			name:    "svLEN",
			v:       NewVariantMock([]string{"svLEN"}),
			wantErr: true,
		},
		{
			name:    "All propercase",
			v:       NewVariantMock([]string{ACMG, PUBMED, SVLEN, CLASS}),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := checkCase(tt.v); (err != nil) != tt.wantErr {
				t.Errorf("checkCase() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
