package info

import (
	"github.com/brentp/vcfgo"
)

// Tokenize
const (
	svDel       = "<DEL>"
	svDup       = "<DUP>"
	svDupTandem = "<DUP:TANDEM>"
)

func isSV(variant *vcfgo.Variant) bool {
	return ValidSV(variant.Alternate[0])
}

func validSV() func(string) bool {
	var svAlleles = map[string]bool{
		svDel:       true,
		svDup:       true,
		svDupTandem: true,
	}
	return func(s string) bool {
		return svAlleles[s]
	}
}

var ValidSV = validSV()
