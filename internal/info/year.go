package info

const (
	minYear = 1950
	maxYear = 3000
)

func ValidYear(year int) bool {
	return year > minYear && year < maxYear
}
