package info

import (
	"testing"

	"gitlab.com/DPIPE/datasharing/varde/pedant/internal/test"
)

func TestIsSV(t *testing.T) {
	tests := []struct {
		name    string
		vcfFile string
		want    bool
	}{
		{
			name:    "Deletion symbolic allele",
			vcfFile: "testdata/info/svlen/del.vcf",
			want:    true,
		},
		{
			name:    "Duplication symbolic allele",
			vcfFile: "testdata/info/svlen/dup.vcf",
			want:    true,
		},
		{
			name:    "Tandem duplication symbolic allele",
			vcfFile: "testdata/info/svlen/dup_tandem.vcf",
			want:    true,
		},
		{
			name:    "INDEL deletion, sequence as alt allele",
			vcfFile: "testdata/info/svlen/indel.vcf",
			want:    false, // TODO: is this ok?
		},
		{
			name:    "SNP",
			vcfFile: "testdata/info/svlen/snp.vcf",
			want:    false,
		},
		{
			name:    "Deletion, no SVLEN",
			vcfFile: "testdata/info/svlen/del_no_SVLEN.vcf",
			want:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isSV(test.VCFFromFile(t, tt.vcfFile).Read()); got != tt.want {
				t.Errorf("validSVLEN() = %v, want %v", got, tt.want)
			}
		})
	}
}
