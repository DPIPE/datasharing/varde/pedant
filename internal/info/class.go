package info

// Tokenize the valid classes
const (
	DR     = "DR"
	NP     = "NP"
	RF     = "RF"
	Class1 = "1"
	Class2 = "2"
	Class3 = "3"
	Class4 = "4"
	Class5 = "5"
)

// validClass creates a map of valid classes and returns a
// function that checks if a given class is valid.
func validClass() func(string) bool {
	var classMap = map[string]bool{
		DR:     true,
		NP:     true,
		RF:     true,
		Class1: true,
		Class2: true,
		Class3: true,
		Class4: true,
		Class5: true,
	}
	return func(class string) bool {
		return classMap[class]
	}
}

// ValidClass checks if a given class is valid
var ValidClass = validClass()
