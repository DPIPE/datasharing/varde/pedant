package info

import (
	"reflect"
	"testing"
)

func Test_ACMGCriterion(t *testing.T) {
	tests := []struct {
		code  string
		valid bool
	}{
		{
			code:  "INVALID",
			valid: false,
		},
		{
			code:  "BS1",
			valid: true,
		},
		{
			code:  "NON-ACMG",
			valid: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.code, func(t *testing.T) {
			if got := validACMGCriterion(tt.code); !reflect.DeepEqual(got, tt.valid) {
				t.Errorf("ACMGCriterion(%v) = %v, want %v", tt.code, got, tt.valid)
			}
		})
	}
}

func Test_ACMGCriteria(t *testing.T) {
	tests := []struct {
		name    string
		codes   interface{}
		wantErr bool
	}{
		{
			name:    "Single valid",
			codes:   "BS1",
			wantErr: false,
		},
		{
			name:    "List valid",
			codes:   []string{"BS1", "NON-ACMG"},
			wantErr: false,
		},
		{
			name:    "Invalid",
			codes:   "INVALID",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidACMGCriteria(tt.codes)
			if (err != nil) != tt.wantErr {
				t.Errorf("%s", err)
			}
		})
	}
}
