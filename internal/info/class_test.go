package info

import (
	"reflect"
	"testing"
)

func Test_Class(t *testing.T) {
	tests := []struct {
		class string
		valid bool
	}{
		{
			class: "1",
			valid: true,
		},
		{
			class: "NP",
			valid: true,
		},
		{
			class: "INVALID",
			valid: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.class, func(t *testing.T) {
			if got := ValidClass(tt.class); !reflect.DeepEqual(got, tt.valid) {
				t.Errorf("ACMGCriteria(%v) = %v, want %v", tt.class, got, tt.valid)
			}
		})
	}
}
