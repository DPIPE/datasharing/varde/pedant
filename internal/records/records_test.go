package records

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	"github.com/brentp/vcfgo"
	"github.com/grailbio/bio/encoding/fasta"
	"gitlab.com/DPIPE/datasharing/varde/pedant/internal/test"
)

func Test_validVariants(t *testing.T) {
	tests := []struct {
		name    string
		vcfFile string
		wantErr bool
	}{
		{
			name:    "Valid multirecord",
			vcfFile: "testdata/records/valid_multirecord.vcf",
			wantErr: false,
		},
		{
			name:    "Not ordered",
			vcfFile: "testdata/records/unordered.vcf",
			wantErr: true,
		},
		{
			name:    "Chrom not mentioned in header",
			vcfFile: "testdata/records/chrom_not_in_header.vcf",
			wantErr: true,
		},
		{
			name:    "Two different variants, same position",
			vcfFile: "testdata/records/same_pos.vcf",
			wantErr: false,
		},
		{
			name:    "Multiallelic variant",
			vcfFile: "testdata/records/multiallelic_variant.vcf",
			wantErr: true,
		},
		{
			name:    "Year is string",
			vcfFile: "testdata/info/info/year_string.vcf",
			wantErr: true,
		},
		{
			name:    "Ref is '.'",
			vcfFile: "testdata/records/invalid_ref_notation.vcf",
			wantErr: true,
		},
		{
			name:    "Same position, different reference alleles",
			vcfFile: "testdata/records/same_pos_different_ref.vcf",
			wantErr: false, // reference fasta check is required to pick up these errors
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if _, err := RecordsValid(test.VCFFromFile(t, tt.vcfFile)); (err != nil) != tt.wantErr {
				t.Errorf("validVariants() returned unwanted error %q", err)
			}
		})
	}
}

func Test_contigIdx(t *testing.T) {
	tests := []struct {
		name    string
		contigs []map[string]string
		want    map[string]int
	}{
		{
			name: "Should be fine",
			contigs: []map[string]string{
				{"ID": "33", "length": "2"},
				{"ID": "2", "length": "32"},
			},
			want: map[string]int{
				"33": 0,
				"2":  1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := contigIdx(tt.contigs); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("contigIdx() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sortAndRefCheck(t *testing.T) {

	tests := []struct {
		name      string
		a         *vcfgo.Variant
		b         *vcfgo.Variant
		contigIdx map[string]int
		want      bool
	}{
		{
			name: "Different contig ordered",
			a: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        0,
			},
			b: &vcfgo.Variant{
				Chromosome: "C",
				Pos:        4,
			},
			contigIdx: map[string]int{
				"A": 0,
				"C": 1,
			},
			want: true,
		},
		{
			name: "Same contig, different coordinate ordered",
			a: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        0,
			},
			b: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        4,
			},
			contigIdx: map[string]int{
				"A": 0,
			},
			want: true,
		},
		{
			name: "Different contig unordered",
			a: &vcfgo.Variant{
				Chromosome: "C",
				Pos:        0,
			},
			b: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        4,
			},
			contigIdx: map[string]int{
				"A": 0,
				"C": 1,
			},
			want: false,
		},
		{
			name: "Same contig, different coordinate unordered",
			a: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        100,
			},
			b: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        4,
			},
			contigIdx: map[string]int{
				"A": 0,
			},
			want: false,
		},
		{
			name: "Identical position",
			a: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        100,
			},
			b: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        100,
			},
			contigIdx: map[string]int{
				"A": 0,
			},
			want: true,
		},
		{
			name: "Contig not in contig map",
			a: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        100,
			},
			b: &vcfgo.Variant{
				Chromosome: "C",
				Pos:        4,
			},
			contigIdx: map[string]int{
				"A": 0,
			},
			want: false,
		},
		{
			name: "First position empty",
			a:    &vcfgo.Variant{},
			b: &vcfgo.Variant{
				Chromosome: "C",
				Pos:        4,
			},
			contigIdx: map[string]int{
				"A": 0,
				"C": 1,
			},
			want: true,
		}, {
			name: "Same position, different ref allele",
			a: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        100,
				Reference:  "C",
			},
			b: &vcfgo.Variant{
				Chromosome: "A",
				Pos:        100,
				Reference:  "G",
			},
			contigIdx: map[string]int{
				"A": 0,
			},
			want: true, // reference fasta check is required to pick up these errors
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := correctOrder(tt.a, tt.b, tt.contigIdx); got != tt.want {
				t.Errorf("correctOrder() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_multipleAltAlleles(t *testing.T) {
	tests := []struct {
		name    string
		variant *vcfgo.Variant
		want    bool
	}{
		{
			name: "Biallelic variant",
			variant: &vcfgo.Variant{
				Alternate: []string{"A"},

				LineNumber: 0,
			},
			want: false,
		},
		{
			name: "Multiallelic variant",
			variant: &vcfgo.Variant{
				Alternate: []string{"A", "AT"},

				LineNumber: 0,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := multiallelic(tt.variant); got != tt.want {
				t.Errorf("multiallelic() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_validNotation(t *testing.T) {
	tests := []struct {
		name    string
		variant *vcfgo.Variant
		want    bool
	}{
		{
			name: "Valid",
			variant: &vcfgo.Variant{
				Reference: "A",
				Alternate: []string{"A"},
			},
			want: true,
		},
		{
			name: "Invalid notation in ref",
			variant: &vcfgo.Variant{
				Reference: ".",
				Alternate: []string{"A"},
			},
			want: false,
		},
		{
			name: "Invalid notation in alt",
			variant: &vcfgo.Variant{
				Reference: "A",
				Alternate: []string{"."},
			},
			want: false,
		},
		{
			name: "Invalid notation in second alt",
			variant: &vcfgo.Variant{
				Reference: "A",
				Alternate: []string{"A", "."},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := validNotation(tt.variant); got != tt.want {
				t.Errorf("validNotation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_checkReferenceSequence(t *testing.T) {
	fastaData := `>seq1
AcGTA
CGTAC
GT
>seq2 A viral sequence
ACGT
ACGT
`
	fa, err := fasta.New(strings.NewReader(fastaData), fasta.OptClean)
	if err != nil {
		t.Errorf("Failed setting up test: %q", err)
	}
	tests := []struct {
		name    string
		variant *vcfgo.Variant
		wantErr bool
	}{
		{
			name: "Ref is correct",
			variant: &vcfgo.Variant{
				Chromosome: "seq1",
				Pos:        10,
				Reference:  "C",
				Alternate:  []string{"G"},
			},
			wantErr: false,
		},
		{
			name: "Ref is incorect",
			variant: &vcfgo.Variant{
				Chromosome: "seq1",
				Pos:        10,
				Reference:  "A",
				Alternate:  []string{"G"},
			},
			wantErr: true,
		},
		{
			name: "Variant has position not in reference",
			variant: &vcfgo.Variant{
				Chromosome: "seq1",
				Pos:        100000000,
				Reference:  "A",
				Alternate:  []string{"G"},
			},
			wantErr: true,
		},
		{
			name: "INDEL",
			variant: &vcfgo.Variant{
				Chromosome: "seq1",
				Pos:        10,
				Reference:  "C",
				Alternate:  []string{"GAAG"},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := checkReferenceSequence(tt.variant, fa); (err != nil) != tt.wantErr {
				t.Errorf("RefCheck() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_CheckReferenceSequence(t *testing.T) {
	fastaData := `>seq1
AcGTA
CGTAC
GT
>15
ACGT
ACGT
`
	tests := []struct {
		name    string
		vcfFile string
		want    []failedRecord
		wantErr bool
	}{
		{
			name:    "CHROM not in fasta",
			vcfFile: "testdata/records/valid_multirecord.vcf",
			want: []failedRecord{
				{Err: fmt.Errorf("variant is out of bounds"), Line: 33},
				{Err: fmt.Errorf("sequence not found: 20"), Line: 34},
				{Err: fmt.Errorf("sequence not found: 21"), Line: 35},
				{Err: fmt.Errorf("sequence not found: 21"), Line: 36}},
			wantErr: true,
		},
		{
			name:    "Same position, different REF",
			vcfFile: "testdata/records/same_pos_different_ref.vcf",
			want: []failedRecord{
				{Err: fmt.Errorf("reference mismatch. REF is A, expected C"), Line: 34}},
			wantErr: true,
		},
		{
			name:    "Two records with same REF",
			vcfFile: "testdata/records/reference_ok.vcf",
			want:    []failedRecord{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(
			tt.name,
			func(t *testing.T) {
				t.Parallel()
				ref := strings.NewReader(fastaData)
				failedRecords, err := CheckReferenceSequence(test.VCFFromFile(t, tt.vcfFile), ref)
				if (err != nil) != tt.wantErr {
					t.Errorf("CheckReferenceSequence() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
				for idx, got := range failedRecords {
					want := tt.want[idx]
					if want.Line != got.Line {
						t.Errorf("Expected linenumbers %q, got, %q", want.Line, got.Line)
					}
					if want.Err.Error() != got.Err.Error() {
						t.Errorf("Fail!!!, %q, %q", got.Err, want.Err)
					}
				}
			},
		)
	}
}
