package records

import (
	"fmt"
	"io"
	"log/slog"
	"slices"

	"github.com/brentp/vcfgo"
	"github.com/grailbio/bio/encoding/fasta"
	"gitlab.com/DPIPE/datasharing/varde/pedant/internal/info"
)

const invalidNotation = "."

func RecordsValid(reader *vcfgo.Reader) (int64, error) {
	slog.Debug("Validating records")
	lastRecord := new(vcfgo.Variant)
	cIdx := contigIdx(reader.Header.Contigs)
	for {
		variant := reader.Read()
		if reader.Error() != nil {
			return variant.LineNumber, reader.Error()
		}
		if variant == nil {
			break
		}
		// Check if chrom is valid contig
		if _, ok := cIdx[variant.Chromosome]; !ok {
			return variant.LineNumber, fmt.Errorf("chrom of record is not a valid contig")
		}
		// Fail if multiallelic variant
		if multiallelic(variant) {
			return variant.LineNumber, fmt.Errorf("variant is not decomposed")
		}
		// Fail if any of the alleles are represented by "."
		if !validNotation(variant) {
			return variant.LineNumber, fmt.Errorf("allele represented by '.'")
		}
		// Check order
		if !correctOrder(lastRecord, variant, cIdx) {
			return variant.LineNumber, fmt.Errorf("records are not ordered")
		}
		lastRecord = variant
		// Check info field of record
		if _, err := info.ValidInfoField(variant); err != nil {
			return variant.LineNumber, err
		}
	}
	slog.Debug("Records valid")
	return 0, nil
}

func contigIdx(contigs []map[string]string) map[string]int {
	var idxMap = make(map[string]int)
	for idx, contig := range contigs {
		idxMap[contig["ID"]] = idx
	}
	return idxMap
}

// Returns false if not a and b is sorted as the order in the contigIdx.
// For identical positions, make sure the reference alleles are identical.
func correctOrder(a, b *vcfgo.Variant, contigIdx map[string]int) bool {
	aContigIdx := contigIdx[a.Chromosome]
	bContigIdx := contigIdx[b.Chromosome]
	switch {
	case a.Chromosome == "":
		return true
	case aContigIdx < bContigIdx:
		return true
	case aContigIdx == bContigIdx && int(a.Pos) <= int(b.Pos):
		return true
	default:
		return false
	}
}

// Returns false if number of alternative alleles is bigger than one
func multiallelic(variant *vcfgo.Variant) bool {
	return len(variant.Alternate) > 1
}

// Returns false if reference or alternative allele is "."
func validNotation(variant *vcfgo.Variant) bool {
	alleles := append(variant.Alternate, variant.Reference)
	return !slices.Contains(alleles, invalidNotation)
}

// Looks up the input variant's position in fasta
// and fails if the fasta sequence is different to variants REF-field
func checkReferenceSequence(variant *vcfgo.Variant, fa fasta.Fasta) error {
	lenght, err := fa.Len(variant.Chromosome)
	if err != nil {
		return err
	}
	if lenght < uint64(variant.Start()) {
		return fmt.Errorf("variant is out of bounds")
	}
	str, err := fa.Get(variant.Chromosome, uint64(variant.Start()), uint64(variant.End()))
	if err != nil {
		return err
	}
	if str != variant.Reference {
		return fmt.Errorf("reference mismatch. REF is %s, expected %s", variant.Reference, str)
	}
	return nil
}

type failedRecord struct {
	Err  error
	Line int
}

// Looks for inconsistencies between sequence in the REF field of a vcf and the reference fasta.
// Returns failing variants line numbers.
func CheckReferenceSequence(reader *vcfgo.Reader, referenceFasta io.Reader) ([]failedRecord, error) {
	var err error
	failedRecords := []failedRecord{}
	slog.Debug("Checking for reference inconsistencies")
	fa, err := fasta.New(referenceFasta)
	if err != nil {
		return failedRecords, err
	}
	for {
		variant := reader.Read()
		if reader.Error() != nil {
			return failedRecords, reader.Error()
		}
		if variant == nil {
			break
		}
		if err = checkReferenceSequence(variant, fa); err != nil {
			failedRecords = append(failedRecords, failedRecord{
				Err:  err,
				Line: int(variant.LineNumber),
			})
		}
	}
	if len(failedRecords) != 0 {
		err = fmt.Errorf("failed reference check")
	}
	return failedRecords, err
}
