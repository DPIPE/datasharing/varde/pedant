module gitlab.com/DPIPE/datasharing/varde/pedant

go 1.23

require (
	github.com/alecthomas/kong v1.6.1
	github.com/brentp/vcfgo v0.0.0-20240930171553-9739269bd784
)

require (
	v.io v0.2.0 // indirect
	v.io/x/lib v0.1.21 // indirect
)

require (
	github.com/grailbio/base v0.0.10-0.20200817015340-8e5f8ec2e457 // indirect
	github.com/grailbio/bio v0.0.0-20200818183458-d966d878d120
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.28.0 // indirect
)

require (
	github.com/brentp/irelate v0.0.1 // indirect
	golang.org/x/exp v0.0.0-20250106191152-7588d65b2ba8
)
